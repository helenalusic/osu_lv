import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.linear_model import LogisticRegression



X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a

colors=['blue', 'red']
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=matplotlib.colors.ListedColormap(colors)) 
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker="x", cmap=matplotlib.colors.ListedColormap(colors))
plt.xlabel('x1')
plt.ylabel('x2')
plt.title("Podaci za treniranje(.) i testiranje(x)")
cbar=plt.colorbar(ticks=[0,1])
plt.show()


#b
model = LogisticRegression(random_state=5)
model.fit(X_train, y_train)


#c
b=model.intercept_  #theta0
w1,w2=model.coef_.T #theta 1 i 2??

#granica odluke:
c = -b/w2
m = -w1/w2

xmin, xmax = X_train[:, 0].min()-1, X_train[:, 0].max()+1
ymin, ymax = X_train[:, 1].min()-1, X_train[:, 1].max()+1




xd = np.array([xmin, xmax])
yd = m*xd + c
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:red', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='tab:blue', alpha=0.2)

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=matplotlib.colors.ListedColormap(colors)) 
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker="x", cmap=matplotlib.colors.ListedColormap(colors))
#plt.xlim(x1x2min, x1x2max) plt.ylim(x1x2min, x1x2max)
plt.xlabel('X1')
plt.ylabel('X2')
plt.legend()
plt.show()

#d
y_pred = model.predict(X_test)

cm = confusion_matrix(y_test, y_pred)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_pred))
disp.plot()
plt.show()

print(" Tocnost : ", accuracy_score(y_test, y_pred))
print(" Odziv : ", recall_score(y_test, y_pred))
print(" Preciznost : ", precision_score(y_test, y_pred))

#e
for i in range(len(y_test)):
    if y_test[i] == y_pred[i]:
        plt.scatter(X_test[i][0], X_test[i][1], color="green")
    else:
        plt.scatter(X_test[i][0], X_test[i][1], color="black")

plt.xlabel('X1')
plt.ylabel('X2')
plt.show()



