import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from keras . models import load_model

model = load_model('/content/ FCA ')
model . summary()

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko krivo klasificiranih slika iz train skupa


misclassified_index = np.where(np.argmax(predictions, axis=1) != y_test)[0]
fig, axs = plt.subplots(1, 5,)
for i, idx in enumerate(misclassified_index[:5]):
    axs[i].imshow(x_test[idx], cmap='gray')
    axs[i].set_title(
        f"Label: {y_test[idx]}, \nPred: {np.argmax(predictions[idx])}")
plt.show()
