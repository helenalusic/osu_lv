'''Zadatak 1.4.4 Napišite Python skriptu koja ´ce uˇcitati tekstualnu datoteku naziva song.txt.
Potrebno je napraviti rjeˇcnik koji kao kljuˇceve koristi sve razliˇcite rijeˇci koje se pojavljuju u
datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijeˇc (kljuˇc) pojavljuje u datoteci.
Koliko je rijeˇci koje se pojavljuju samo jednom u datoteci? Ispišite ih.'''

word_dict={}
fhand=open("song.txt")
for line in fhand :
    line = line.rstrip () 
    line=line.lower()
    words = line.split ()
    for word in words:
        try:
            word_dict[word]=word_dict[word]+1
        except:
            word_dict[word]=1
        
count=0
for word in word_dict:
    if word_dict[word]==1:
        count+=1
        print(word)
    

print(count)