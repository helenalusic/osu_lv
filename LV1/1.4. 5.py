'''Zadatak 1.4.5 Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ SMSSpamCollection.txt
[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozna ˇ cene kao ˇ spam, a neke kao ham.
Primjer dijela datoteke:'''

spam= 0
ham=0

uskl = 0
brspam=0
brham=0

fhand = open('SMSSpamCollection.txt')

for line in fhand:
    line = line.rstrip()
    
    if line.startswith("ham"):
        brham+=1
        ham += len(line.replace("ham",'').split())
    elif line.startswith("spam"):
        brspam+=1
        spam+= len(line.replace("spam",'').split())
        if line.endswith('!'):
            uskl+=1

print("spam:" + str(spam/brspam))
print("ham:" + str(ham/brham))
print("usklicnici:" + str(uskl))
