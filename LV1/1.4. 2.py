'''Zadatak 1.4.2 Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
nekakvu ocjenu i nalazi se izme ¯ du 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju
sljede´cih uvjeta:'''
try:
    point=float(input("Unesite ocjenu: "))

    if point>=0.9 and point<=1.0:
        grade="A"
    elif point<0.9 and point>=0.8:
        grade="B"
    elif point<0.8 and point>=0.7:
        grade="C"
    elif point<0.7 and point>=0.6:
        grade="D"
    elif point <0.6:
        grade="F"
    print(grade)

except:
    print("Niste unijeli ocjenu :(")
