import numpy as np
import matplotlib . pyplot as plt 

data = np.loadtxt("data.csv",skiprows=1,delimiter=",")
#a
print(len(data))

#b
height=np.array(data[:,1])
weight=np.array(data[:,2])
plt.xlabel ('height')
plt.ylabel ('weight')
plt.scatter(height,weight)
plt.show()


#c
height50=np.array(data[::50,1]) #start:stop:step
weight50=np.array(data[::50,2])
plt.xlabel ('height')
plt.ylabel ('weight')
plt.scatter(height50,weight50)
plt.show()

#d
print ( np.mean(height))
print ( np.max(height))
print ( np.min(height))

#e
indf = (data[:,0] == 0)
heightf=np.array(data[:,1][indf])
print ( np.mean(heightf))
print ( np.max(heightf))
print ( np.min(heightf))

indm = (data[:,0] == 1)
heightm=np.array(data[:,1][indm])
print ( np.mean(heightm))
print ( np.max(heightm))
print ( np.min(heightm))