import numpy as np
import matplotlib . pyplot as plt

black=np.zeros((50,50))
white=np.ones((50,50))

first=np.vstack((black,white))
second=np.vstack((white,black))
img=np.hstack((first,second))
plt.figure()
plt.imshow(img, cmap="gray")
