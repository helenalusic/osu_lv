import numpy as np
import matplotlib . pyplot as plt

img=plt.imread("road.jpg")

img = img [:,:,0]. copy ()

plt.figure ()
plt.imshow ( img, cmap ="gray")
plt.show ()

#a
brightness=img+20
plt.imshow (brightness, cmap ="gray")
plt.show ()

#b
columns=img[1,:].size
img2ndquart=img[:int(columns/4),:int(columns/2)]
plt.imshow (img2ndquart, cmap ="gray")
plt.show()

#c
ninetydegree=np.rot90(img,-1)
plt.imshow (ninetydegree, cmap ="gray")
plt.show()

#d
flipped=np.flip(img, axis=1)
plt.imshow (flipped, cmap ="gray")
plt.show()
